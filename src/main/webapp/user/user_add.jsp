<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>用户添加</title>
    <link href="../css/bootstrap.min.css?v=3.3.6"  rel="stylesheet">
    <link href="../css/font-awesome.css?v=4.4.0"  rel="stylesheet">
    <link href="../css/animate.css"  rel="stylesheet">
    <link href="../css/style.css?v=4.1.0" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="col-sm-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>管理员添加</h5>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal m-t" action="${APP_PATH}/user/add.action" method="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">用户名：</label>
                        <div class="col-sm-8">
                            <input id="username" name="user.username" class="form-control" type="text">
                            <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> ${requestScope.msg}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">姓名：</label>
                        <div class="col-sm-8">
                            <input id="name" name="user.name" class="form-control" type="text" aria-required="true" aria-invalid="false" class="valid">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">邮箱：</label>
                        <div class="col-sm-8">
                            <input id="email" name="user.email" class="form-control" type="text" aria-required="true" aria-invalid="true" class="error">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">电话：</label>
                        <div class="col-sm-8">
                            <input id="tel" name="user.tel" class="form-control" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">出生日期：</label>
                        <div class="col-sm-8">
                            <input id="birthday" name="user.birthday" class="form-control" type="date">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">密码：</label>
                        <div class="col-sm-8">
                            <input id="password" name="user.password" class="form-control" type="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">性别：</label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                                <input type="radio" name="user.sex" id="male" value="男"> 男
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="user.sex" id="female" value="女"> 女
                            </label>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button class="btn btn-primary" type="submit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>



<script src="../js/jquery.min.js?v=2.1.4"></script>
<script>
    $(function () {
        // 校验用户名是否存在
        $("#username_input").change(function() {
            // 发送ajax请求验证用户名是否可用
            var username = this.value;
            $.ajax({
                url:"user/" + username,
                type:"GET",
                success:function(result) {
                    if (result.code == 200) {
                        show_validate_msg("#username_input", "error", "用户已存在");
                        $("#user_add").attr("ajax-va", "error");
                    } else {
                        show_validate_msg("#username_input", "success", "用户名可用");
                        $("#user_add").attr("ajax-va", "success");
                    }
                }
            });
        });

        // 显示校验结果的提示信息
        function show_validate_msg(ele,status,msg) {
            // 清空原有校验状态
            $(ele).parent().removeClass("has-error has-success");
            $(ele).next("span").text();
            if ("success" == status) {
                // 添加新元素
                $(ele).parent().addClass("has-success");
                $(ele).next("span").text(msg);
            } else if("error" == status) {
                $(ele).parent().addClass("has-error");
                $(ele).next("span").text(msg);
            }
        }
        
        $("#user_add").click(function () {
            var pwd1 = $("#pwd1").val()
            var pwd2 = $("#pwd2").val()
            if (pwd1 != pwd2) {
                alert("两次密码不一致！")
                return false
            }
            var form = $("#user_form").serialize()
            $.ajax({
                url: "user",
                data: form,
                type: "POST",
                success: function (result) {
                    alert(result.extend.msg)
                    window.location.reload()
                }
            })
        })
    })
</script>

</html>
