<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户注册</title>
    <link href="../css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css?v=4.1.0" rel="stylesheet">
    <script>if(window.top !== window.self){ window.top.location = window.location;}</script>
</head>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1>注册</h1>
            </div>
            <p>创建一个新账户</p>
            <p style="color: red">${sessionScope.msg}</p>
            <form class="m-t" role="form" action="${APP_PATH}/user/register.action" method="post">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="请输入用户名" name="user.username" required="">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="请输入真实姓名" name="user.name" required="">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="请输入邮箱" name="user.email" required="">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="请输入电话" name="user.tel" required="">
                </div>
                <div class="form-group">
                    <input type="date" class="form-control" placeholder="请选择出生日期" name="user.birthday" required="">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="请输入密码" name="user.password" required="">
                </div>
                <label class="radio-inline">
                    <input type="radio" name="user.sex" id="male" value="男"> 男
                </label>
                <label class="radio-inline">
                    <input type="radio" name="user.sex" id="female" value="女"> 女
                </label>
                <div class="form-group text-left">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="checkbox" id="agree" name="agree">我同意注册协议
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">注 册</button>

                <p class="text-muted text-center"><small>已经有账户了？</small><a href="login.jsp">点此登录</a>
                </p>
            </form>
        </div>
    </div>

    <!-- 全局js -->
    <script src="../js/jquery.min.js?v=2.1.4"></script>
    <script src="../js/bootstrap.min.js?v=3.3.6"></script>

</body>

</html>
