<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户管理</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <link href="../css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <!-- Panel Other -->
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3>用户管理</h3>
        </div>
        <div class="ibox-content">
            <div class="row row-lg">
                <div class="col-sm-12">
                    <!-- Example Pagination -->
                    <div class="example-wrap">
                        <div class="example">
                            <table data-toggle="table" data-mobile-responsive="true">
                                <thead>
                                <tr>
                                    <th data-checkbox="true"></th>
                                    <th>ID</th>
                                    <th>用户名</th>
                                    <th>姓名</th>
                                    <th>电话</th>
                                    <th>邮箱</th>
                                    <th>出生日期</th>
                                    <th>性别</th>
                                    <th>状态</th>
                                    <th>权限</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${requestScope.list}" var="user">
                                    <tr>
                                        <td><input type="checkbox" checked="checked" id="${user.id}"></td>
                                        <td>${user.id}</td>
                                        <td>${user.username}</td>
                                        <td>${user.name}</td>
                                        <td>${user.tel}</td>
                                        <td>${user.email}</td>
                                        <td>${user.birthday}</td>
                                        <td>${user.sex}</td>
                                        <td>${user.state == 1 ? "已激活":"未激活"}</td>
                                        <td>${user.authority == 1 ? "管理员":"用户"}</td>
                                        <td>
                                            <%--<button class="btn btn-info btn-sm edit_btn" id="${user.id}">--%>
                                                <%--<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑--%>
                                            <%--</button>--%>
                                            <button class="btn btn-danger btn-sm del_btn" id="${user.id}">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true" ></span>删除
                                            </button>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End Example Pagination -->
                </div>

            </div>
        </div>
    </div>
    <!-- End Panel Other -->
</div>
<!-- 全局js -->
<script src="../js/jquery.min.js?v=2.1.4"></script>
<script src="../js/bootstrap.min.js?v=3.3.6"></script>
<!-- 自定义js -->
<script src="../js/content.js?v=1.0.0"></script>
<!-- Bootstrap table -->
<script src="../js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="../js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
<script src="../js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<script>
    $(function () {
       // 删除用户
        $(document).on("click", ".del_btn", function() {
           var id = $(this).attr('id');
           var username = $(this).parents("tr").find("td:eq(2)").text();
           if (confirm("需确认删除" + username + "吗？")) {
               // 发送ajax请求
               $.ajax({
                   url: "${APP_PATH}/user/delete.do",
                   type: "POST",
                   data: "id="+id,
                   success: function (result) {
                       alert(result);
                       window.location.reload();
                   }
               });
           }
       }); 
        
    });
</script>
</body>
</html>
