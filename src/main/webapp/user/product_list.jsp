<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    pageContext.setAttribute("APP_PATH", request.getContextPath());
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>商品管理</title>
    <link href="../css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="../css/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3>商品管理</h3>
        </div>
        <div class="ibox-content">
            <div class="row row-lg">
                <div class="col-sm-12">
                    <!-- Example Pagination -->
                    <div class="example-wrap">
                        <div class="example">
                            <table data-toggle="table" data-mobile-responsive="true">
                                <thead>
                                <tr>
                                    <th data-checkbox="true"></th>
                                    <th>#</th>
                                    <th>名称</th>
                                    <th>剩余数量</th>
                                    <th>市场价格</th>
                                    <th>商城价格</th>
                                    <th>是否热销</th>
                                    <th>分类</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><input type="checkbox" checked="checked"></td>
                                    <td>1</td>
                                    <td>Java EE实战教程</td>
                                    <td>5</td>
                                    <td>89.3</td>
                                    <td>55.2</td>
                                    <td>是</td>
                                    <td>Java</td>
                                    <td>
                                        <button class="btn btn-info btn-sm reset_btn">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑
                                        </button>
                                        <button class="btn btn-danger btn-sm del_btn">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" checked="checked"></td>
                                    <td>1</td>
                                    <td>Java EE实战教程</td>
                                    <td>5</td>
                                    <td>89.3</td>
                                    <td>55.2</td>
                                    <td>是</td>
                                    <td>Java</td>
                                    <td>
                                        <button class="btn btn-info btn-sm reset_btn">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑
                                        </button>
                                        <button class="btn btn-danger btn-sm del_btn">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" checked="checked"></td>
                                    <td>1</td>
                                    <td>Java EE实战教程</td>
                                    <td>5</td>
                                    <td>89.3</td>
                                    <td>55.2</td>
                                    <td>是</td>
                                    <td>Java</td>
                                    <td>
                                        <button class="btn btn-info btn-sm reset_btn">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑
                                        </button>
                                        <button class="btn btn-danger btn-sm del_btn">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" checked="checked"></td>
                                    <td>1</td>
                                    <td>Java EE实战教程</td>
                                    <td>5</td>
                                    <td>89.3</td>
                                    <td>55.2</td>
                                    <td>是</td>
                                    <td>Java</td>
                                    <td>
                                        <button class="btn btn-info btn-sm reset_btn">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑
                                        </button>
                                        <button class="btn btn-danger btn-sm del_btn">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" checked="checked"></td>
                                    <td>1</td>
                                    <td>Java EE实战教程</td>
                                    <td>5</td>
                                    <td>89.3</td>
                                    <td>55.2</td>
                                    <td>是</td>
                                    <td>Java</td>
                                    <td>
                                        <button class="btn btn-info btn-sm reset_btn">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑
                                        </button>
                                        <button class="btn btn-danger btn-sm del_btn">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" checked="checked"></td>
                                    <td>1</td>
                                    <td>Java EE实战教程</td>
                                    <td>5</td>
                                    <td>89.3</td>
                                    <td>55.2</td>
                                    <td>是</td>
                                    <td>Java</td>
                                    <td>
                                        <button class="btn btn-info btn-sm reset_btn">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑
                                        </button>
                                        <button class="btn btn-danger btn-sm del_btn">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" checked="checked"></td>
                                    <td>1</td>
                                    <td>Java EE实战教程</td>
                                    <td>5</td>
                                    <td>89.3</td>
                                    <td>55.2</td>
                                    <td>是</td>
                                    <td>Java</td>
                                    <td>
                                        <button class="btn btn-info btn-sm reset_btn">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>编辑
                                        </button>
                                        <button class="btn btn-danger btn-sm del_btn">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>删除
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- End Example Pagination -->
                </div>

            </div>
        </div>
    </div>
</div>

<!-- 全局js -->
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/content.js"></script>
<script src="../js/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="../js/plugins/bootstrap-table/bootstrap-table-mobile.min.js"></script>
<script src="../js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
</body>

</html>
