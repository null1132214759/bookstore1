package com.edu.bookstore.service;

import com.edu.bookstore.dao.UserDao;
import com.edu.bookstore.entity.User;
import com.edu.bookstore.util.MD5Util;
import com.edu.bookstore.util.MailUtils;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Random;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-20
 * Time: 19:46
 * Description: 用户Service
 */
public class UserService {
    
    UserDao userDao = new UserDao();

    /**
     * 用户注册
     * @param user
     * @param request
     * @return
     */
    public int insert(final User user, HttpServletRequest request) {
        String password = MD5Util.MD5(user.getPassword());
        Random random = new Random();
        // 随机生成四位数验证码
        String code = String.valueOf(random.nextInt(9000) + 1000);
        Integer state = 0;
        user.setCode(code);
        user.setState(state);
        user.setPassword(password);
        int i = 0;
        try {
            i = userDao.insert(user);
        } catch (Exception e) {  // 若数据插入过程产生异常，直接返回0
            return 0;
        }
        // 邮件发送激活链接
        String url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort() +  request.getContextPath()
                + "/user/active.do?email=" + user.getEmail() + "&code=" + code;
        final String msg = "亲爱的： "+user.getName()+" 先生/女士!  <br/> 欢迎注册我们的图书商城!点击下面链接激活<br/>"
                + "<a href='"+ url +"'>点击激活</a>"
                + "无法打开请复制链接: " + url;
        // 开新线程用于邮件发送，增强用户体验
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MailUtils.sendMail(user.getEmail(), msg);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
        });
//        thread.start();
        return i;
    }

    /**
     * 添加管理员
     * @return
     */
    public int insertOne(User user) {
        int i = userDao.insert(user);
        return i;
    }

    /**
     * 用户删除
     * @param id
     * @return
     */
    public int delete(Integer id) {
        System.out.println(id);
        int i = userDao.delete(User.class, id);
        return i;
    }

    /**
     * 用户查询
     * @param id
     * @return
     */
    public User select(Integer id) {
        User user = userDao.findById(User.class, id);
        return user;
    }

    /**
     * 查询全部用户
     * @return
     */
    public List<User> list() {
        List<User> list = userDao.list();
        return list;
    }

    /**
     * 用户更新
     * @param user
     * @return
     */
    public int update(User user) {
        int i = userDao.update(user);
        return i;
    }

    /**
     * 用户激活
     * @param email
     * @param code
     * @return
     */
    public int active(String email, String code) {
        int i = userDao.active(email, code);
        return i;
    }

    /**
     * 用户登录
     * @param email
     * @param username
     * @param password
     * @return
     */
    public User login(String email, String username, String password) {
        username = email;
        User user1 = userDao.selectByEmail(email);
        User user2 = userDao.selectByUsername(username);
        if (user1 != null) {
            return user1;
        } else {
            return user2;
        }
    }

    /**
     * 更具username与email判断用户是否存在
     * @param user
     * @return
     */
    public boolean isExit(User user) {
        User user1 = userDao.selectByUsername(user.getUsername());
        User user2 = userDao.selectByEmail(user.getEmail());
        if (user1 == null && user2 == null) {
            return false;
        }
        return true;
    }
}
