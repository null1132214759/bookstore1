package com.edu.bookstore.entity;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-26
 * Time: 8:35
 * Description: 购物车
 */
public class Cart {
    
    private Double totalCount;  // 购物车总金额
    
    private Map<Integer, CartItem> map = new LinkedHashMap<>();  // 购物车子项

    public Cart(Double totalCount, Map<Integer, CartItem> map) {
        this.totalCount = totalCount;
        this.map = map;
    }

    public Cart() {
    }

    public Double getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Double totalCount) {
        this.totalCount = totalCount;
    }

    public Map<Integer, CartItem> getMap() {
        return map;
    }

    public void setMap(Map<Integer, CartItem> map) {
        this.map = map;
    }

    // 将单个商品添加到购物车
    public void add(CartItem item) {
        //判断map中是否存在
        Integer id = item.getProduct().getId();
        if (map.containsKey(id)) {
            //1.获取新的数量
            int newC = item.getCount();
            //2.获取老的购物车项
            CartItem oldItem = map.get(id);
            //最终的数量
            int zC = oldItem.getCount() + newC;
            //设置给老的购物车项
            oldItem.setCount(zC);
        } else {
            map.put(id, item);
        }
        //修改总金额
        totalCount += item.getTotalCount();
    }
    
    // 购物车移除商品
    public void remove(Integer id) {
        CartItem item = map.remove(id);
        totalCount -= item.getTotalCount();
    }
    
    // 清空购物车
    public void clear() {
        map.clear();
        totalCount = 0.0;
    }
}
