package com.edu.bookstore.entity;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-26
 * Time: 8:22
 * Description:
 */
public class Order {
    
    private Integer id;
    
    private Integer orderId;  // 订单编号：根据一定规则生成
    
    private Date time;  // 订单生成时间
    
    private Double total;  // 订单总额
    
    private String name;  // 收件人姓名
    
    private String tel;  // 收件人电话
    
    private String address;  // 收件人地址
    
    private String expressCode;  // 快递编号
    
    private String expressComp;  // 快递公司
    
    private Integer state;  // 订单状态 1.未支付 2.已支付 3.已发货
    
    private User user;  // 属于那个用户

    //包含那些订单项
    private List<Item> items = new LinkedList<>();

    public Order(Integer id, Integer orderId, Date time, Double total, String name, String tel, String address, String expressCode, String expressComp, Integer state, User user, List<Item> items) {
        this.id = id;
        this.orderId = orderId;
        this.time = time;
        this.total = total;
        this.name = name;
        this.tel = tel;
        this.address = address;
        this.expressCode = expressCode;
        this.expressComp = expressComp;
        this.state = state;
        this.user = user;
        this.items = items;
    }

    public Order() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getExpressCode() {
        return expressCode;
    }

    public void setExpressCode(String expressCode) {
        this.expressCode = expressCode;
    }

    public String getExpressComp() {
        return expressComp;
    }

    public void setExpressComp(String expressComp) {
        this.expressComp = expressComp;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", time=" + time +
                ", total=" + total +
                ", name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", address='" + address + '\'' +
                ", expressCode='" + expressCode + '\'' +
                ", expressComp='" + expressComp + '\'' +
                ", state=" + state +
                ", user=" + user +
                ", items=" + items +
                '}';
    }
}
