package com.edu.bookstore.entity;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-26
 * Time: 8:17
 * Description:
 */
public class Product {
    
    private Integer id;
    
    private String name;
    
    private String describe;
    
    private Integer num;  // 商品剩余数量
    
    private Double marketPrice; // 市场价格
    
    private Double shopPrice;  // 商城价格
    
    private String images; // 商品图片
    
    private Integer isHot;  // 是否热销商品
    
    private Integer cId;  // 分类id

    public Product(Integer id, String name, String describe, Integer num, Double marketPrice, Double shopPrice, String images, Integer isHot, Integer cId) {
        this.id = id;
        this.name = name;
        this.describe = describe;
        this.num = num;
        this.marketPrice = marketPrice;
        this.shopPrice = shopPrice;
        this.images = images;
        this.isHot = isHot;
        this.cId = cId;
    }

    public Product() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Double marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Double getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(Double shopPrice) {
        this.shopPrice = shopPrice;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", describe='" + describe + '\'' +
                ", num=" + num +
                ", marketPrice=" + marketPrice +
                ", shopPrice=" + shopPrice +
                ", images='" + images + '\'' +
                ", isHot=" + isHot +
                ", cId=" + cId +
                '}';
    }
}
