package com.edu.bookstore.entity;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-26
 * Time: 8:33
 * Description: 购物车项
 */
public class CartItem {
    
    private Product product;  // 商品
    
    private Integer count;  // 总数


    public CartItem(Product product, Integer count) {
        this.product = product;
        this.count = count;
    }

    public CartItem() {
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    
    public Double getTotalCount() {
        return product.getShopPrice() * count;
    }
    
}
