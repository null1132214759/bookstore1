package com.edu.bookstore.entity;

import java.util.Date;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-20
 * Time: 19:46
 * Description: 用户实体类
 */
public class User {
    
    private Integer id;
    
    private String username;
    
    private String password;
    
    private String name;
    
    private String tel;
    
    private String email;
    
    private Date birthday;
    
    private String sex;  // 男||女
    
    private Integer state;  // 用户状态: 未激活0  已激活1
    
    private String code;  // 用户激活验证码
    
    private Integer authority; // 用户权限 管理员1  买家2

    public User() {
    }

    public User(Integer id, String username, String password, String name, String tel, String email, Date birthday, String sex, Integer state, String code, Integer authority) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.tel = tel;
        this.email = email;
        this.birthday = birthday;
        this.sex = sex;
        this.state = state;
        this.code = code;
        this.authority = authority;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getAuthority() {
        return authority;
    }

    public void setAuthority(Integer authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", tel='" + tel + '\'' +
                ", email='" + email + '\'' +
                ", birthday=" + birthday +
                ", sex='" + sex + '\'' +
                ", state=" + state +
                ", code='" + code + '\'' +
                ", authority=" + authority +
                '}';
    }
}
