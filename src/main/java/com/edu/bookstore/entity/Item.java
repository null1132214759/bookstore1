package com.edu.bookstore.entity;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-26
 * Time: 8:27
 * Description:
 */
public class Item {
    
    private Integer id;
    
    private Order order;  // 属于那个订单
    
    private Product product;  // 包含那个商品
    
    private Integer count;
    
    private Double total;

    public Item(Integer id, Order order, Product product, Integer count, Double total) {
        this.id = id;
        this.order = order;
        this.product = product;
        this.count = count;
        this.total = total;
    }

    public Item() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", order=" + order +
                ", product=" + product +
                ", count=" + count +
                ", total=" + total +
                '}';
    }
}
