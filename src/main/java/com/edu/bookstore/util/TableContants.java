package com.edu.bookstore.util;

public class TableContants {

    public static final String USERTABLE = "user";

    public static final String PRODUCT = "product";

    public static final String CATEGORY = "category";

    public static final String ORDER = "order";

    public static final String ITEM = "item";

    public static final String USER_SESSSION = "user_session";
    
    public static final String SUCCESS = "success";
    
    public static final String FAILURE = "failure";
}
