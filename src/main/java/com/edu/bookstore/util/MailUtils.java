package com.edu.bookstore.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import java.util.Properties;

public class MailUtils {

	public static void sendMail(String email, String emailMsg)
			throws MessagingException {

		// 设置邮箱服务器，邮箱需开启POP3/SMTP服务
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.163.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "25");
		// 创建验证器
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						//邮箱用户名和密码，密码为授权码（通过qq邮箱获取）        
						return new PasswordAuthentication("nildeveloper@163.com","test123456");
					}
				});

		// 2.创建一个Message，它相当于是邮件内容
		Message message = new MimeMessage(session);
		//设置发送者 全路径
		message.setFrom(new InternetAddress("nildeveloper@163.com"));
		//设置发送方式与接收者
		message.setRecipient(RecipientType.TO, new InternetAddress(email));
		//设置邮件主题
		message.setSubject("用户激活");
		// message.setText("这是一封激活邮件，请<a href='#'>点击</a>");
		//设置邮件内容 
		message.setContent(emailMsg, "text/html;charset=utf-8");
		// 3.创建 Transport用于将邮件发送
		//接收放的地址
		Transport.send(message);
	}
	
}
