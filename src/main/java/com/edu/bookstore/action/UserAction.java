package com.edu.bookstore.action;

import com.edu.bookstore.entity.User;
import com.edu.bookstore.service.UserService;
import com.edu.bookstore.util.MD5Util;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.edu.bookstore.util.TableContants.USER_SESSSION;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-20
 * Time: 19:46
 * Description:用户Action
 */
public class UserAction extends ActionSupport {

    private User user;
    private String msg;
    private Integer id;
    private String email;
    private String code;

    UserService userService = new UserService();

    ActionContext context = ActionContext.getContext();
    Map session = context.getSession();
    HttpServletRequest request = ServletActionContext.getRequest();

    /**
     * 用户注册
     */
    public String register() {
        String forward = null;
        boolean exit = userService.isExit(user);
        if (exit) {
            msg = "邮箱或用户名已存在，请重新尝试！";
            session.put("msg", msg);
            return "failure";
        }
        int result = userService.insert(user, request);
        if (result > 0) {
            forward = "success";
            msg = "注册成功！赶紧去邮箱激活吧！";
        } else {
            forward = "failure";
            msg = "注册失败！清再次尝试！";
        }
        session.put("msg", msg);
        return forward;
    }

    /**
     * 用户登录
     *
     * @return
     */
    public String login() {
        String forward = null;
        String md5Password = MD5Util.MD5(this.user.getPassword());
        User user1 = userService.login(this.user.getEmail(), this.user.getUsername(), md5Password);
        if (user1 == null) {
            forward = "failure";
            msg = "用户不存在，请先注册！";
        } else {
            if (!user1.getPassword().equals(md5Password)) {
                forward = "failure";
                msg = "密码错误，请重新输入！";
            } else if (user1.getState() == 0) {
                System.out.println("==========" + user1.getState());
                forward = "failure";
                msg = "未激活，请激活后再次尝试登陆！";
            } else {
                forward = "success";
                session.put(USER_SESSSION, user1);  // 登录用户保存到session
            }
        }
        session.put("msg", msg);
        return forward;
    }

    /**
     * 添加用户
     *
     * @return
     */
    public String insert() {
        String forward = null;
        boolean exit = userService.isExit(user);
        if (exit) {
            msg = "邮箱或用户名已存在，请重新尝试！";
            session.put("msg", msg);
            return "failure";
        }
        // 添加管理员直接设置其权限即可
        user.setState(1);
        user.setAuthority(1);
        int result = userService.insertOne(user);
        if (result > 0) {
            msg = "添加成功！";
            forward = "success";
        } else {
            msg = "添加失败！";
            forward = "failure";
        }
        request.setAttribute("msg", msg);
        return forward;
    }

    /**
     * 用户激活
     *
     * @return
     */
    public String active() {
        int i = userService.active(email, code);
        String msg1 = "";
        String msg2 = "";
        String code = "200";
        if (i > 0) {
            msg1 = "激活成功！";
            msg2 = "赶紧去登录吧！";
        } else {
            code = "100";
            msg1 = "激活失败！";
            msg2 = "再试试吧！";
        }
        session.put("msg1", msg1);
        session.put("msg2", msg2);
        session.put("code", code);
        return "success";
    }

    /**
     * 根据id查询用户
     *
     * @return
     */
    public String select() {
        String forward = null;
        User user = userService.select(id);
        if (user == null) {
            forward = "failure";
            msg = "查询失败！";
        } else {
            session.put("user", user);
        }
        session.put("msg", msg);
        return forward;
    }

    /**
     * 根据id删除用户
     */
    public void delete() {
        int i = userService.delete(id);
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/plain;charset=UTF-8");
        try {
            if (i > 0) {
                msg = "删除成功！";
            } else {
                msg = "删除失败！";
            }
            response.getWriter().print(msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 更具id更新用户
     *
     * @return
     */
    public String update() {
        String forward = null;
        int i = userService.update(user);
        if (i > 0) {
            forward = "success";
        } else {
            forward = "failure";
        }
        return forward;
    }

    /**
     * 查询全部用户
     *
     * @return
     */
    public String list() {
        String forward = null;
        List<User> list = userService.list();
        request.setAttribute("list", list);
        forward = "success";
        return forward;
    }

    /**
     * 用户退出
     *
     * @return
     */
    public String logout() {
        session.remove(USER_SESSSION);
        msg = "退出成功！";
        session.put("msg", msg);
        return "success";
    }
    
    public String forward() {
        return "success";
    }
    

    // getter and setter
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
