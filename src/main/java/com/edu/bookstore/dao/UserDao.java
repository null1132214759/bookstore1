package com.edu.bookstore.dao;

import com.edu.bookstore.entity.User;
import com.edu.bookstore.util.JdbcPoolUtil;
import com.edu.bookstore.util.TableContants;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created with IntellJ IDEA.
 * User: lhl
 * Date: 2018-10-20
 * Time: 19:46
 * Description: 用户DAO
 */
public class UserDao implements IBaseDao<User>{

    protected static final String USER = "username, password, name, tel, email, birthday, sex, authority, state, code";

    /**
     * 用户注册
     * @param user
     * @return
     */
    @Override
    public int insert(User user) {
        String sql = "insert into " + TableContants.USERTABLE
                + "(" + USER + ")" + " values(?,?,?,?,?,?,?,?,?,?);";
        // 参数
        Object[] params = {user.getUsername(), user.getPassword(), user.getName(), user.getTel(), user.getEmail(),
            user.getBirthday(), user.getSex(), user.getAuthority(), user.getState(), user.getCode()};
        int i = JdbcPoolUtil.update(sql, params);
        return i;
    }

    @Override
    public int insertList(List<User> list) {
        return 0;
    }

    /**
     * 根据id更新用户
     * @param user
     * @return
     */
    @Override
    public int update(User user) {
        String sql = "update user set username = ?, password = ?, name = ?, tel = ?," +
                "birthday = ?, sex = ? where id = ?;";
        Object[] params = {user.getUsername(), user.getPassword(), user.getName(), user.getTel(),
                user.getBirthday(), user.getSex(), user.getId()};
        int i = JdbcPoolUtil.update(sql, params);
        return i;
    }

    @Override
    public int deleteList(Class<User> c, int... ids) {
        return 0;
    }

    @Override
    public int delete(User o) {
        return 0;
    }

    /**
     * 根据id删除用户
     * @param c
     * @param id
     * @return
     */
    @Override
    public int delete(Class<User> c, int id) {
        String sql = "delete from " + TableContants.USERTABLE + " where id = ?";
        int i = JdbcPoolUtil.update(sql, id);
        return i;
    }

    /**
     * 根据id查询用户
     * @param c
     * @param id
     * @return
     */
    @Override
    public User findById(Class<User> c, int id) {
        String sql = "select user from " + TableContants.USERTABLE + " where id = ?;";
        Connection connection = null;
        ResultSet rs = null;
        User user = null;
        try {
            connection = JdbcPoolUtil.getConnect();
            rs = JdbcPoolUtil.query(connection, sql);
            while (rs.next()) {
                Integer id1 = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String email = rs.getString("email");
                Date birthday = rs.getDate("birthday");
                String sex = rs.getString("sex");
                Integer authority = rs.getInt("authority");
                Integer state = rs.getInt("state");
                String code = rs.getString("code");
                user = new User(id1, username, password, name, tel, email, birthday, sex, state, code, authority);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcPoolUtil.close(rs, null, connection);
        }
        return user;
    }

    /**
     * 查询全部用户
     * @return
     */
    @Override
    public List<User> list() {
        String sql = "select * from " + TableContants.USERTABLE;
        List<User> list = new ArrayList<>();
        Connection connection = null;
        ResultSet rs = null;
        try {
            connection = JdbcPoolUtil.getConnect();
            rs = JdbcPoolUtil.query(connection, sql);
            while (rs.next()) {
                Integer id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String email = rs.getString("email");
                Date birthday = rs.getDate("birthday");
                String sex = rs.getString("sex");
                Integer authority = rs.getInt("authority");
                Integer state = rs.getInt("state");
                String code = rs.getString("code");
                User user = new User(id, username, password, name, tel, email, birthday, sex, state, code, authority);
                list.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcPoolUtil.close(rs, null, connection);
        }
        return list;
    }

    @Override
    public List<User> findByCondition(String condition) {
       return null;
    }

    /**
     * 根据email查询用户
     * @param email
     * @return
     */
    public User selectByEmail(String email) {
        String sql = "select * from " + TableContants.USERTABLE + " where email = ?";
        Object[] params = {email};
        Connection connection = null;
        ResultSet rs = null;
        User user = null;
        try {
            connection = JdbcPoolUtil.getConnect();
            rs = JdbcPoolUtil.query(connection, sql, params);
            while (rs.next()) {
                Integer id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String email1 = rs.getString("email");
                Date birthday = rs.getDate("birthday");
                String sex = rs.getString("sex");
                Integer authority = rs.getInt("authority");
                Integer state = rs.getInt("state");
                String code = rs.getString("code");
                user = new User(id, username, password, name, tel, email1, birthday, sex, state, code, authority);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * 根据用户名查询用户
     * @param username
     * @return
     */
    public User selectByUsername(String username) {
        String sql = "select * from " + TableContants.USERTABLE + " where username = ?";
        Object[] params = {username};
        Connection connection = null;
        ResultSet rs = null;
        User user = null;
        try {
            connection = JdbcPoolUtil.getConnect();
            rs = JdbcPoolUtil.query(connection, sql, params);
            while (rs.next()) {
                Integer id = rs.getInt("id");
                String username1 = rs.getString("username");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String tel = rs.getString("tel");
                String email = rs.getString("email");
                Date birthday = rs.getDate("birthday");
                String sex = rs.getString("sex");
                Integer authority = rs.getInt("authority");
                Integer state = rs.getInt("state");
                String code = rs.getString("code");
                user = new User(id, username1, password, name, tel, email, birthday, sex, state, code, authority);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    /**
     * 用户激活
     * @param email
     * @param code
     * @return
     */
    public int active(String email, String code) {
        String sql = "update user set state = 1, code = null where email = ? and code = ?";
        Object[] params = {email, code};
        int i = JdbcPoolUtil.update(sql, params);
        return i;
    }
}
